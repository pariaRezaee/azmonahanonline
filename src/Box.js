import React from 'react'
import './Box.css'
import './ForBtn'
import pic from './Chrysanthemum.jpg'
import $ from 'jquery'

export const Box = () => {
    const hiddenBtn = () =>{
        $('#btnRead').hide();
    }

    const showBtn = () =>{
        $('#btnRead').show();
    }

    return (
        <div>
            <div className="box-yellow"></div>
            <div className="box-orange"></div>
            <div>
                <div className="container">
                    <p className="animate">01</p>
                    <div className="text">
                        <h4>AhanOnline</h4>
                        <p>Lorem ipsum dolor sit amet
                        Consectetur adipisicing elit. Ea id facere ipsam recusandae unde
                        assumenda ad atque nihil natus error,
                        quis ducimus,official pariatur porro,
                        quod perspiciatis et quam eaque!
                        </p>
                    </div>
                    <div>
                        <button className="btnRead" id="btnRead">Read more</button>
                    </div>
                </div>
            </div>
            <img src={pic} className="pic-box" alt="pic" />
            <div className="position">
            <button onClick={() =>{hiddenBtn()}} className="btn btn-outline-danger">Hide</button>
            <button onClick={() =>{showBtn()}} className="btn btn-outline-success show">Show</button>
            </div>
        </div>

    )
}
